import React, { useState } from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { logo_icon, down_icon, up_icon, back_image } from "../../assets/images";
import { TouchableOpacity } from "react-native-gesture-handler";


export const CardComponent = (props) => {
    const { image, title, subTitle, isOpen, onPress, showArrow, imageStyle, titleStyle, subTitleStyle, containerStyle, imageContainerStyle } = props
    return <TouchableOpacity onPress={onPress} style={[styles.cardContainer, containerStyle]}>
        <View style={imageContainerStyle}>
            <Image source={image} height={45} width={45}  style={[{ height: 45, width: 45 }, imageStyle]}/>
        </View>
        <View style={styles.contentSection}>
            <Text style={[titleStyle]}>{title}</Text>
            <Text style={[subTitleStyle]}>{subTitle}</Text>
        </View>
        {showArrow ? <Image source={isOpen ? up_icon : down_icon} style={styles.arrowStyle} /> : null}
    </TouchableOpacity>
}

export const ColapseCardComponent = (props) => {
    const { data, isOpenInit, headerImageColor } = props
    const [isOpen, setIsOpen] = useState(isOpenInit)
    if (!(data && data.length > 0))
        return null

    return <View style={{ borderRadius: 10, backgroundColor: '#fff', marginBottom: 20, padding: 5,marginHorizontal: 30, }}>
        <CardComponent
            imageContainerStyle={{
                backgroundColor: headerImageColor,
                padding: 10,
                width: 50, height: 50,
                borderRadius: 50,
                justifyContent: 'center',
                alignItems: 'center'
                // borderWidth:1,
                // borderStyle:'solid',
                // borderColor: '#fff'
            }}
            imageStyle={{
                width: 25,
                height: 25,
            }}
            titleStyle={{
                fontSize: 17,
                fontFamily: 'Poppins-Bold',
            }}
            subTitleStyle={{ fontSize: 13, color: '#ababab', fontWeight: '600', paddingRight: 16 }}
            containerStyle={{ padding: 10 }}
            title={data[0].title}
            subTitle={data[0].subTitle}
            image={data[0].image}
            isOpen={isOpen}
            showArrow
            onPress={() => {
                setIsOpen(!isOpen)
            }} />
        {data.length > 1 && isOpen ? <>
            <CardComponent containerStyle={styles.innerContent}
                titleStyle={{
                    fontSize: 15,
                }}
                subTitleStyle={{ fontSize: 13, color: '#ababab', fontWeight: '600', paddingRight: 16 }}
                title={data[1].title}
                subTitle={data[1].subTitle}
                image={data[1].image}
                isOpen={isOpen}
                imageStyle={{
                    borderRadius: 50,
                }}
            />
            <>
                {data.length > 2 ? <>
                    <CardComponent containerStyle={styles.innerContent}
                        titleStyle={{
                            fontSize: 15,
                            fontFamily: 'Poppins-Bold',
                        }}
                        subTitleStyle={{ fontSize: 13, color: '#ababab', fontWeight: '600', paddingRight: 16 }}
                        title={data[2].title}
                        subTitle={data[2].subTitle}
                        image={data[2].image}
                        isOpen={isOpen}
                        imageStyle={{
                            borderRadius: 50,
                        }}
                    />
                    <Text style={{ textAlign: "right", color: 'blue', paddingRight: 15, paddingBottom: 10, }}>View more</Text>
                </> : null}
            </>

        </> : null}
    </View>
}

const styles = StyleSheet.create({
    cardContainer: {
        padding: 10,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        paddingBottom: 14,
    },
    contentSection: {
        paddingLeft: 20,
        flex: 1,
    },
    arrowStyle: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    innerContent: {
        borderTopWidth: 1,
        borderStyle: 'solid',
        borderColor: '#F0F0F0',
        paddingBottom: 14,
    }
})