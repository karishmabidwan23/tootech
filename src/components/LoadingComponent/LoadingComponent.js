import React from 'react'
import { ActivityIndicator, View, StyleSheet, Dimensions, ProgressBarAndroid } from 'react-native';
import { connect } from 'react-redux';

const Loading = (props) => {
  const { loading } = props
  return <>
    {loading ?

      <ProgressBarAndroid size="large" styleAttr={"Horizontal"} progress={100} style={{ height: 20, width: '100%' }} />

      : null}
  </>
}

const mapStateToProps = state => {
  return {
    loading: state.global.loading
  }
}


export default LoadingComponent = connect(mapStateToProps)(Loading) 