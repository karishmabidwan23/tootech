import React, { PureComponent } from 'react';
import { View, Text as GMText, Dimensions, StyleSheet } from 'react-native';
import NetInfo from "@react-native-community/netinfo";

const { width } = Dimensions.get('window');

function MiniOfflineSign() {
    return (
        <View style={styles.offlineContainer}>
            <GMText style={styles.offlineText}>No Internet Connection</GMText>
        </View>
    );
}
var unsubscribe
class OfflineNotice extends PureComponent {
    state = {
        isConnected: true
    };

    componentDidMount() {
        unsubscribe = NetInfo.addEventListener(this.handleConnectivityChange);
    }

    componentWillUnmount() {
        if (unsubscribe)
            unsubscribe()
    }

    handleConnectivityChange = state => {
        this.setState({ isConnected: state.isConnected });
    };

    render() {
        if (!this.state.isConnected) {
            return <MiniOfflineSign />;
        }
        return null;
    }
}

const styles = StyleSheet.create({
    offlineContainer: {
        backgroundColor: '#b52424',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width,
        position: 'absolute',
        top: 0,
        zIndex: 9
    },
    offlineText: { color: '#fff' }
});

export default OfflineNotice;