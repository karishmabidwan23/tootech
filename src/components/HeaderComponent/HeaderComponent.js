import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';



export const HeaderComponent = (props) => {

    const { back_image, show_menu, navigation } = props;
    return <>
        <SafeAreaView>
            <StatusBar
                hidden={true}
                barStyle="light-content" translucent={true} backgroundColor="transparent" />

            <View style={styles.headerWrapper}>

                {back_image ? <TouchableOpacity onPress={() => { navigation.goBack() }} style={styles.backIconStyle}>
                    <Image source={back_image} resizeMode='center' style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                </TouchableOpacity>
                    : null}
                {show_menu ? <TouchableOpacity style={styles.menuIconStyle}>
                    <Image source={show_menu} resizeMode='center' style={{ width: hp(3), height: hp(3), resizeMode: 'contain' }} />
                </TouchableOpacity>
                    : null}
            </View>
        </SafeAreaView>
    </>
}


const styles = StyleSheet.create({
    headerWrapper: {
        flexDirection: 'row',
        height: 50,
        width: '100%',
    },
    headerBackgroundStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 12,
        width: '100%'
    },
    backIconStyle: {
        width: hp(2),
        height: hp(2.5),
        resizeMode: 'contain',
    },
    menuIconStyle: {
        paddingLeft: hp(2.5),
        marginTop: hp(5)
    },
})