import React, { useEffect } from "react";
import { View, Alert } from "react-native";
import { connect } from "react-redux";
import { setErrorAction } from "../../redux/actions/GlobalActions";

const ErrorComponent = (props) => {
    useEffect(() => {
        if (props.error) {
            Alert.alert("Error", props.error ? props.error.message || props.error : "Something went wrong try again.",[{"text":"Ok",onPress:() => {props.dispatch(setErrorAction(undefined))}}])
        }
    }, [props.error])

    return <>
    </>
}

export default connect((state) => ({
    error: state.global.error
}))(ErrorComponent)