import React from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { verification_image } from '../../assets/images';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { fontsize_24, fontsize_regular, fontsize_medium } from '../../styles/CommonFonts';
import PhoneInput from 'react-native-phone-input'
import * as AuthActionCreator from '../../redux/actions/AuthActions';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import RouterConst from '../../navigation/RouterConst';


class GenerateOtpScreen extends React.Component {

    constructor(props) {
        super(props)
        const { dispatch } = props
        this.boundActionCreators = bindActionCreators(AuthActionCreator, dispatch)
    }

    onPressGenerate = () => {
        const phone = this.phone.getValue().slice(3)
        const countryCode ='+'+ this.phone.getCountryCode()
        this.boundActionCreators.generateOtp(phone, countryCode)
    }

    static getDerivedStateFromProps = (props, state) => {
        GenerateOtpScreen.navigateIfOtpDataPresent(props)
    }

    static navigateIfOtpDataPresent = (props) => {
        if(props.otpData)
           props.navigation.navigate(RouterConst.OtpSignInScreen)
    }

    componentDidMount() {
        GenerateOtpScreen.navigateIfOtpDataPresent(this.props)
        this.setState({
            pickerData: this.phone.getPickerData()
        })
        this.phone.selectCountry("IN")
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View style={styles.pageWrapper}>
                        <Image source={verification_image} style={{ width: 200, height: 200, resizeMode: 'contain' }} />
                        <Text style={styles.optTitle}>OTP Verification</Text>
                        <Text style={styles.messageTitle}>we will send you an <Text style={styles.titleBold}>one time password</Text> on this mobile number.</Text>
                        <PhoneInput style={styles.phoneWrapper}
                            ref={(ref) => { this.phone = ref; }}
                            initialCountry={'in'}
                            flagStyle={{
                                width: 50,
                                height: 30
                            }}
                            textStyle={{
                                fontSize: fontsize_medium,
                                marginLeft: hp(4)
                            }}
                        />
                        <TouchableOpacity style={styles.buttonStyle} onPress={this.onPressGenerate}>
                            <Text style={styles.textStyle}>Generate OTP</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }


}


export default connect(state => ({ otpData: state.auth.otpData }))(GenerateOtpScreen)

const styles = StyleSheet.create({
    pageWrapper: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: hp(5),

    },
    optTitle: {
        fontSize: fontsize_24 + 1,
        fontFamily: 'Poppins-Bold',
        color: '#000',
        marginTop: hp(3),
        fontWeight: '600',
    },
    messageTitle: {
        fontSize: fontsize_regular + 1,
        color: '#909090',
        fontFamily: 'Poppins-Medium',
        textAlign: 'center',
        fontWeight: '500',
        marginTop: hp(3),
        lineHeight: hp(3)
    },
    titleBold: {
        color: "#000",
        fontWeight: '700',
        textTransform: 'capitalize'
    },
    phoneWrapper: {
        marginTop: hp(5),
        backgroundColor: '#F5F5F5',
        height: 45,
        paddingHorizontal: hp(2.5),
        fontSize: fontsize_medium
    },
    buttonStyle: {
        backgroundColor: '#04252f',
        width: '100%',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp(7)

    },
    textStyle: {
        color: '#fff',
        fontSize: fontsize_medium - 1,
        fontFamily: 'Poppins-Medium',

    }

})
