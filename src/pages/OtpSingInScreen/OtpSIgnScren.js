import React from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { verification_image } from '../../assets/images';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { fontsize_24, fontsize_regular, fontsize_medium } from '../../styles/CommonFonts';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import * as AuthActionCreator from '../../redux/actions/AuthActions';
import RouterConst from '../../navigation/RouterConst';
import { StackActions } from '@react-navigation/native';



class OtpScreen extends React.Component {

    state = {
        code: ''
    }

    constructor(props) {
        super(props)
        const { dispatch } = props
        this.boundActionCreators = bindActionCreators(AuthActionCreator, dispatch)
    }

    static getDerivedStateFromProps = (props, state) => {
        OtpScreen.navigateIfAuthDataPresent(props)
    }

    static navigateIfAuthDataPresent = (props) => {
        if (props.authData)
            props.navigation.dispatch(StackActions.replace(RouterConst.HOMESCREEN))
    }

    componentDidMount() {
        if (!this.props.otpData)
            this.props.navigation.goBack()
    }

    onVerify = (code) => {
        this.boundActionCreators.verifySignInWithOtp(code, this.props.otpData.country_code, this.props.otpData.contact_number)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View style={styles.pageWrapper}>
                        <Image source={verification_image} style={{ width: 200, height: 200, resizeMode: 'contain' }} />
                        <Text style={styles.optTitle}>OTP Verification</Text>
                        <Text style={styles.messageTitle}>Enter OTP send to  <Text style={styles.titleBold}>+91 7878787878</Text></Text>
                        <OTPInputView
                            style={{ width: '80%', height: 200 }}
                            pinCount={4}
                            autoFocusOnLoad
                            codeInputFieldStyle={styles.underlineStyleBase}
                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                            onCodeFilled={(code) => { this.setState({ code }) }}
                        />
                        <TouchableOpacity style={styles.buttonStyle} onPress={() => {
                            this.onVerify(this.state.code)
                        }}>
                            <Text style={styles.textStyle}>Verify</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }


}

export default connect(state => ({ authData: state.auth.authData, otpData: state.auth.otpData }))(OtpScreen);

const styles = StyleSheet.create({
    pageWrapper: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: hp(5),
        paddingTop: hp(10)
    },
    optTitle: {
        fontSize: fontsize_24 + 1,
        fontFamily: 'Poppins-Bold',
        color: '#000',
        marginTop: hp(3),
        fontWeight: '600',
    },
    messageTitle: {
        fontSize: fontsize_regular + 1,
        color: '#909090',
        fontFamily: 'Poppins-Medium',
        textAlign: 'center',
        fontWeight: '500',
        marginTop: hp(3),
        lineHeight: hp(3)
    },
    titleBold: {
        color: "#000",
        fontWeight: '700',
        textTransform: 'capitalize'
    },
    phoneWrapper: {
        marginTop: hp(5),
        backgroundColor: '#F5F5F5',
        height: 45,
        paddingHorizontal: hp(2.5),
        fontSize: fontsize_medium
    },
    buttonStyle: {
        backgroundColor: '#04252f',
        width: '100%',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp(2)

    },
    textStyle: {
        color: '#fff',
        fontSize: fontsize_medium - 1,
        fontFamily: 'Poppins-Medium',

    },
    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "#04252f",
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: '#04252f'
    },

    underlineStyleHighLighted: {
        borderColor: "#04252f",
    },

})
