import React from 'react';
import { SafeAreaView, ScrollView, ImageBackground, StyleSheet, View, Text, Image } from 'react-native';
import { home_image, menu_icon, logo_icon, user_icon, clock_icon, mutli_user_icon, print_icon } from '../../assets/images';
import { HeaderComponent } from '../../components/HeaderComponent/HeaderComponent';
import { fontsize_22, fontsize_regular, fontsize_large } from '../../styles/CommonFonts';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as HomeActionCreator from '../../redux/actions/HomeActions';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { ColapseCardComponent } from '../../components/ColapseCard/ColapseCardComponent';


class HomeScreen extends React.Component {

    constructor(props) {
        super(props)
        const { dispatch } = props
        this.boundActionCreators = bindActionCreators(HomeActionCreator, dispatch)
    }


    static getDerivedStateFromProps = (props, state) => {
        const { homeData } = props
        const checkIns = [{ image: clock_icon, title: "Checkin History", subTitle: homeData ? "Last Checked in " + new Date(homeData.last_check_in).toISOString() : "" }]
        const guestReq = [{ image: mutli_user_icon, title: "Guest Request", subTitle: homeData ? homeData.guest_request_count + " Requests" : "" }]
        const guestIn = [{ image: user_icon, title: "Manage Guests", subTitle: homeData ? "Total" + homeData.total_guest + " Guests" : "" }]
        if (homeData) {
            const { image_prefix, check_in, guest_request, guest, last_check_in, guest_request_count, total_guest } = homeData
            if (check_in)
                check_in.map(ch => {
                    checkIns.push({ title: ch.landlord ? ch.landlord.name : "", subTitle: ch.updated_at, image: ch && ch.landlord ? { uri: image_prefix + "/" + ch.landlord.logo } : logo_icon })
                })
            if (guest_request)
                guest_request.map(gr => {
                    guestReq.push({
                        image: logo_icon,
                        title: gr.guest_name ? gr.guest_name.first_name : "",
                        subTitle: gr.guest_name ? gr.guest_name.last_name : ""
                    })
                })
            if (guest)
                guest.map(gr => {
                    guestIn.push({ image: logo_icon, title: gr.tenant ? gr.tenant.name : "", subTitle: gr.comment })
                })
        }
        return {
            checkIns,
            guestReq,
            guestIn
        }
    }

    componentDidMount() {
        this.boundActionCreators.getHomeData()
    }

    render() {
        const {
            checkIns,
            guestReq,
            guestIn } = this.state
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <ImageBackground source={home_image} resizeMode='cover' style={styles.pageWrapper}>
                        <HeaderComponent show_menu={menu_icon} />
                        <View style={styles.PageTileSection}>
                            <Text style={styles.pageTitle}>welcome,</Text>
                            <Text style={styles.subTitle}>Jhone Doe</Text>
                        </View>
                        <View style={styles.bottomSection}>
                            <View style={styles.bottomStyle}>
                                <View style={styles.imgSection}>
                                    <Image source={logo_icon} style={{ resizeMode: 'contain', alignItems: 'center', justifyContent: 'center', }} />
                                </View>
                                <Text style={styles.companyTitle}>ABC Company Ltd</Text>
                                <Text style={styles.subCompanyTitle}>the hearest tower</Text>
                            </View>
                            <View style={styles.collapseContainer}>
                                <ScrollView contentContainerStyle={{flexGrow:1}}>
                                <ColapseCardComponent data={checkIns} isOpenInit headerImageColor={'#fff4e5'} />
                                <ColapseCardComponent data={guestReq} headerImageColor={'#fff1f4'} image={user_icon} />
                                <ColapseCardComponent data={guestIn} headerImageColor={'#effcf1'} />
                                </ScrollView>
                            </View>
                            <View style={styles.printSection}>
                                <Image source={print_icon}/>
                            </View>
                        </View>
                    </ImageBackground>
                    </ScrollView>
            </SafeAreaView>
        );
    }
}

export default connect(state => ({ authData: state.auth.authData, homeData: state.home.homeData }))(HomeScreen)


const styles = StyleSheet.create({
    pageWrapper: {
        flex: 1,
        width: '100%',
        position: 'relative',
    },
    PageTileSection: {
        marginTop: hp(2),
        padding: hp(2.5)
    },
    pageTitle: {
        color: "#fff",
        fontSize: fontsize_22,
        fontFamily: 'Poppins-Bold',
        textTransform: 'capitalize',
        fontWeight: '700'
    },
    subTitle: {
        color: "#fff",
        fontFamily: 'Poppins-Bold',
        fontSize: fontsize_regular,
        fontWeight: '700'
    },
    bottomSection: {
        flex: 1,
        justifyContent: 'flex-end'

    },
    bottomStyle: {
        backgroundColor: '#fff',
        borderTopLeftRadius: hp(3.5),
        borderTopRightRadius: hp(3.5),
        padding: hp(3),
        position: 'relative'
    },
    imgSection: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: hp('50%'),
        height: 90,
        width: 90,
        position: 'absolute',
        top: -45,
        left: '45%',
        zIndex: 999

    },
    companyTitle: {
        fontSize: fontsize_large,
        fontFamily: 'Poppins-Bold',
        textAlign: 'center',
        marginTop: hp(2.6)
    },
    subCompanyTitle: {
        fontFamily: 'Poppins-Bold',
        textAlign: 'center',
        color: '#ababab',
        textTransform: 'capitalize'
    },
    collapseContainer: {
        backgroundColor: '#f5f5f5',
        paddingVertical: 20,
        paddingBottom: hp(8),
        height: hp(57)

    },
    printSection:{
        backgroundColor:'#0076fe',
        borderRadius: 50,
        width: 65,
        height: 65,
        justifyContent:'center',
        alignItems:'center',
        position: 'absolute',
        bottom: 5,
        left: '44%',
    }
})