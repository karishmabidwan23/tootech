import { createStore,combineReducers,applyMiddleware } from 'redux'
import { HomeReducer } from './reducer/HomeReducer'
import { AuthReducer } from './reducer/AuthReducer'
import { GlobalReducer } from './reducer/GlobalReducer'
import thunkMiddleware from 'redux-thunk'

const rootReducer = combineReducers({
    home:HomeReducer,
    auth:AuthReducer,
    global:GlobalReducer
});

export const store = createStore(rootReducer,applyMiddleware(thunkMiddleware))

console.log(store.getState())
