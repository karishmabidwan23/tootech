import { setLoadingAction, setErrorAction } from "./GlobalActions"
import { doPostWithUrlEncode, HOME_URL } from "../../api"

export const SAVE_HOME_RESPONSE_TYPE = "SAVE_HOME_RESPONSE"
export const setHomeResponseAction = (data) => {
    return {
        type: SAVE_HOME_RESPONSE_TYPE,
        data
    }
}


export const getHomeData = () => {
    return function (dispatch,getState) {
        var authState = getState().auth
        var accessToken = authState.authData.access_token
        return doPostWithUrlEncode(HOME_URL, {Authorization:`Bearer ${accessToken}`}, {})
            .then(data => dispatch(setHomeResponseAction(data)))
            .catch(err => dispatch(setErrorAction(err)))
            .finally(() => dispatch(setLoadingAction(false)))
    }
}