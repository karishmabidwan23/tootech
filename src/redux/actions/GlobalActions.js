


export const SET_LOADING = "SET_LOADING_TYPE"
export const setLoadingAction=(loading)=> {
    return {
        type:SET_LOADING,
        data:loading
    }
}



export const SET_ERROR = "SET_ERROR_TYPE"
export const setErrorAction=(error)=> {
    return {
        type:SET_ERROR,
        data:error
    }
}