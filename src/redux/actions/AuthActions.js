import { setLoadingAction, setErrorAction } from "./GlobalActions";
import { GENERATE_OTP_URL, doPostWithUrlEncode, VERIFY_SIGN_IN_URL } from "../../api";


export const SAVE_AUTH_DATA_TYPE = "SAVE_AUTH_DATA";
export const saveAuthDataAction = (data) => {
    return {
        type: SAVE_AUTH_DATA_TYPE,
        data
    }
}

export const SAVE_OTP_DATA = "SAVE_OTP_DATA";
export const saveOtpDataAction = (data) => {
    return {
        type: SAVE_OTP_DATA,
        data
    }
}


export const generateOtp = (contact_number, country_code) => {
    return function (dispatch) {
        dispatch(setLoadingAction(true))
        return doPostWithUrlEncode(GENERATE_OTP_URL, undefined, { contact_number, country_code })
            .then(data => dispatch(saveOtpDataAction(data)))
            .catch(err => dispatch(setErrorAction(err)))
            .finally(() => dispatch(setLoadingAction(false)))
    }
}


export const verifySignInWithOtp = (otp, country_code, contact_number, device_type = "android", device_token = "1234") => {
    return function (dispatch) {
        dispatch(setLoadingAction(true))
        return doPostWithUrlEncode(VERIFY_SIGN_IN_URL, undefined, { contact_number, country_code, otp, device_type, device_token })
            .then(data => dispatch(saveAuthDataAction(data)))
            .catch(err => dispatch(setErrorAction(err)))
            .finally(() => dispatch(setLoadingAction(false)))
    }
}