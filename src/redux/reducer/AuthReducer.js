import { SAVE_AUTH_DATA_TYPE, SAVE_OTP_DATA } from "../actions/AuthActions";

const initialState = {
    authData: undefined,
    otpData: undefined
}

export function AuthReducer(state = initialState, action) {
    console.log(JSON.stringify(action))
    switch (action.type) {
        case SAVE_AUTH_DATA_TYPE:
            return Object.assign({}, state, { authData: action.data })
        case SAVE_OTP_DATA:
            return Object.assign({}, state, { otpData: action.data.data })
        default:
            return state;
    }
}