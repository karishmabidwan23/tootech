import { SET_LOADING, SET_ERROR } from "../actions/GlobalActions";

const initialState = {
    loading: false,
    error: false
}

export function GlobalReducer(state = initialState, action) {  
    switch (action.type) {
        case SET_LOADING:
            return Object.assign({}, state, { loading: action.data })
        case SET_ERROR:
            return Object.assign({}, state, { error: action.data })
        default:
            return state;
    }
}