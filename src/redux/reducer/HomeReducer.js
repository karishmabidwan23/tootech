import { SAVE_HOME_RESPONSE_TYPE } from "../actions/HomeActions";


const initialState = {
    homeData:undefined
}

export function HomeReducer(state = initialState, action){
    switch(action.type){
        case SAVE_HOME_RESPONSE_TYPE:
            return Object.assign({},state,{homeData:action.data.data})
        default:
            return state;
    }
}