import { RFValue } from "react-native-responsive-fontsize";

const standardScreenHeight = 640;
export const fontsize = RFValue(12, standardScreenHeight)
export const fontsize_regular = RFValue(14, standardScreenHeight)
export const fontsize_medium = RFValue(16, standardScreenHeight)
export const fontsize_large = RFValue(18, standardScreenHeight)
export const fontsize_20 = RFValue(20, standardScreenHeight)
export const fontsize_22 = RFValue(22, standardScreenHeight)
export const fontsize_24 = RFValue(24, standardScreenHeight)
export const fontsize_26 = RFValue(26, standardScreenHeight)
export const fontsize_28 = RFValue(28, standardScreenHeight)
export const fontsize_30 = RFValue(30, standardScreenHeight)
export const fontsize_32 = RFValue(32, standardScreenHeight)
export const fontsize_34 = RFValue(34, standardScreenHeight)
export const fontsize_40 = RFValue(40, standardScreenHeight)