import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RouterConst from '../navigation/RouterConst';
import GenerateOtpScreen  from '../pages/GenerateOtp/GenerateOtp';
import OtpScreen from '../pages/OtpSingInScreen/OtpSIgnScren';
import HomeScreen from '../pages/HomeScreen/HomeScreen';

const RouterStack = createStackNavigator();

export default () => {
    return < NavigationContainer>
        <RouterStack.Navigator initialRouteName={RouterConst.GenerateOtpScreen}>
            <RouterStack.Screen name={RouterConst.GenerateOtpScreen} component={GenerateOtpScreen} options={{ headerShown: false }}/>
            <RouterStack.Screen name={RouterConst.OtpSignInScreen} component={OtpScreen} options={{ headerShown: false }}/>
            <RouterStack.Screen name={RouterConst.HOMESCREEN} component={HomeScreen} options={{ headerShown: false }}/>
        </RouterStack.Navigator>
    </NavigationContainer>
}


