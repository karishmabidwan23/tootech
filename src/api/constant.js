
export const BASE_API_URL="https://trootechproducts.com:8020/api/user/";
export const GENERATE_OTP_URL = BASE_API_URL+'generate-otp/';
export const VERIFY_SIGN_IN_URL = BASE_API_URL+'login/';
export const HOME_URL = BASE_API_URL+'home/';
