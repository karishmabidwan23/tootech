

const axios = require('axios')
const qs = require('querystring')

export const doPostWithUrlEncode = (url, header = {}, body) => new Promise(function (resolve, reject) {
    var data = qs.stringify(body);
    var config = {
        method: 'post',
        url: url,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            ...header
        },
        data: data
    };
    axios(config)
        .then(response => {
            const rd = response.data
            if (rd.status == 200)
                resolve(rd)
            else
               throw new Error(rd.message)
    })
        .catch(err => reject(err))

});