/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import { SafeAreaView, View } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import RouterNavigation from './src/navigation/RouterNavigation';
import { Provider } from 'react-redux'
import { store } from './src/redux';
import LoadingComponent from './src/components/LoadingComponent/LoadingComponent';
import ErrorComponent from './src/components/ErrorComponent/ErrorComponent';
import OfflineNotice from './src/components/OfflineNotice/OfflineNotice';

const App = () => {
  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000)
  }, []);
  return (
    <Provider store={store}>

      <SafeAreaView style={{ flex: 1 }}>
        <ErrorComponent />
        <View style={{ flex: 1, position: 'relative' }}>
          <LoadingComponent />

          <RouterNavigation />
        </View>
      </SafeAreaView>
      <OfflineNotice />
    </Provider>
  );
};

export default App;
